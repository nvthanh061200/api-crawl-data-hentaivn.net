const mongoose = require('mongoose');

async function connect(){
    try {
        await mongoose.connect('mongodb://localhost:27017/HenTai', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        });
        console.log('connect database successfully');
    }catch(err) {
        console.log('connect database failed');
    }
}
module.exports= {connect};