const C_list=require('../Controller/C.list');
const C_test=require('../Controller/test');

const express = require('express');
const routes=express.Router();


routes.post('/TL',C_list.PostTheLoai);
routes.get('/TL',C_list.GetTheLoai);

routes.post('/CT',C_list.PostChiTiet);
routes.get('/CT',C_list.GetChiTiet);

routes.get('/tkid',C_list.GetChiTietTheoId);
routes.post('/Detailf1',C_list.PostDetailf1);

routes.get('/GetDetailf1',C_list.GetDetailf1);
routes.post('/PostLinkImg',C_list.PostLinkImg);

routes.get('/test',C_list.getTest);
routes.get('/test2',C_list.GetImage);

//-----------Tim kiem--------------------------------
routes.get('/Search_Category',C_list.Search_Category); // tim kiem the loai
routes.get('/Search_Story',C_list.Search_Story); // tim kiem the id truyen



//---------------routes test tim kiem or, and, nhieu điều kiện, doc ghi file-----------------
routes.post('/CreateDataTest',C_test.CreateDataTest); // tim kiem the id truyen
routes.get('/timkiem_username',C_test.timkiem_username); // tim kiem the id truyen
routes.get('/timkiem_nhohon',C_test.timkiem_nhohon); // tim kiem nho hon
routes.post('/update_data',C_test.update_data); // update_data
routes.delete('/removedata',C_test.removedata); // delete datamarketplace
routes.get('/fs',C_test.fs); // delete datamarketplace
routes.put('/renameCollision',C_test.renameCollision); // delete datamarketplace
//--------delete------------------------
routes.delete('/deleteCategory',C_list.deleteCategory); // delete datamarketplace

//-----------------rename collision------------------------
routes.put('/reanmeCollision2',C_list.reanmeCollision2);

module.exports=routes;

