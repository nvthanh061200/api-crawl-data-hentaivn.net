const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tam = new Schema({
  username: String,
  age: Number,
  address: String,
  dtb:Number,
  birthday: Date,
  permission: Number,
});
module.exports =mongoose.model('tam',tam);