const cheerio = require('cheerio');
var request=require("request"); 
const puppeteer = require('puppeteer');
const Detail= require('../Model/Detail');
const Category= require('../Model/Category');
const Detail_F2=require('../Model/Detail.F2');
const Tam=require('../Model/tam');
const fs=require('fs');

const { json } = require('body-parser');

module.exports={
    // tao du lieu test
    //[POST] /HenTai/CreateDataTest
    CreateDataTest: async function (req, res){
        try {
            let dem=0;
        for(let i=0;i<5;i++){
            let obj=Tam({
                username:`To Hong Hanh`,
                age:20,
                address:`address${i}`,
                dtb:10,
                birthday:`12/6/2000`,
                permission:1
            })
            let checkSave=await obj.save();
            if(checkSave!=null) {
                dem++;
            }
        }
        res.status(200).json({
            data:dem,
            message:`successfully`
        })
        } catch (error) {
            res.status(500).json({
                message:error.message
            })
        }
    },
    //tim kiem user= username150, age=150
    //[get] HenTai/timkiem_username
    timkiem_username:async function (req, res) {
        let username = req.body.username;
        let checkUserName=await Tam.find({ username: username});
        // let age = req.body.age;
        // let checkUserName=await Tam.find({ age: age});
        if(checkUserName!=null) {
            res.status(200).json({ 
                message: "sucsessfully",
                data:checkUserName,
            })
        }else{
            res.status(500).json({
                message: "username khong ton tai"
            })
        }
    },
    // tim kiem tuoi nho hon
    //[get] HenTai/timkiem_nhohon
    timkiem_nhohon:async function (req, res) {
        let checkUserName=await Tam.find().sort({age:-1});
  
        if(checkUserName!=null) {
            res.status(200).json({ 
                message: "sucsessfully",
                data:checkUserName,
            })
        }else{
            res.status(500).json({
                message: "username khong ton tai"
            })
        }
    },
    //update data
    //[post] //HenTai/update_data
    update_data:async function (req, res) {
        let fiter={
            username:"To Hong Hanh"
        };
        let update={
            $set:{
                username:'To Hong Hanh2'
            }
        }
        let checkUserName=await Tam.updateMany(fiter, update);

        if(checkUserName!=null) {
            res.status(200).json({ 
                message: "sucsessfully",
                data:checkUserName,
            })
        }else{
            res.status(500).json({
                message: "username khong ton tai"
            })
        }
    },
    //[delete] /HenTai/removedata
    removedata:async function (req, res) {
        try {
            let checkDelete=await Tam.remove({ 
                username: "To Hong Hanh"
            })
            if(checkDelete!=null){
                res.status(200).json({
                    message: checkDelete
                })
            }else{
                res.status(500).json({
                    message: "delete failed"
                })
            }
        } catch (error) {
            res.status(500).json({
                message: error.message
            })
        }
    },
    fs:function(req, res, next) {
        // doc file
        // fs.readFile('code.html','utf8',function(err, data){
        //     res.status(200).json({
        //         message: data
        //     })
        // })
        // ghi file
        let content="noi dung 2"
        fs.writeFile('ghifile.txt',content,function(err, data){
            if(!err){
                res.status(200).json({
                    message: "ghi thanh cong",
                })
            }
        })
    },
    //[put] /HenTai/renameCollision đổi tên collision trong mongodb
    renameCollision: async function (req, res) {
       let ss=await Detail.updateMany( {}, { $rename: { "id_father": "Id_TheLoai" } } )
       if(ss!=null) {
            res.status(200).json({
                message: 'doi collision thanh cong',
            })
       }else{
           res.status(500).json({
               message: 'doi collision that bai',
           })
       }
    }
}
