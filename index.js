const bodyParser = require('body-parser');
const express = require('express');
const app = express(); 
const port=5858;
const routes=require('./Routes/index');
const connect=require('./Config/Connect.database');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

connect.connect();
routes(app);

app.listen(port, ()=>{
    console.log(`listening ${port}`);
})
